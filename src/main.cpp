#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <functional>

int parse_arguments(const int argc, const char ** argv, std::string & filename, size_t & numbilets, std::string & parameter) {
    if (argc <= 6) {
        std::cerr << "Error: not all parametres given" << std::endl;
        return -1;
    }
    if (std::string(argv[1]) != "--file") {
        std::cerr << "Error: first parameter must be \"--filename\"" << std::endl; 
        return -2;
    }
    if (std::string(argv[3]) != "--numbilets") {
        std::cerr << "Error: third parameter must be \"--numbilets\"" << std::endl; 
        return -2;
    }
    if (std::string(argv[5]) != "--parameter") {
        std::cerr << "Error: five parameter must be \"--parameter\"" << std::endl; 
        return -3;
    }

    filename = std::string(argv[2]);
    parameter = std::string(argv[6]);

    std::stringstream numbilets_string;        //
    numbilets_string << std::string(argv[4]);  // convert char* argument
    numbilets_string >> numbilets;             // to size_t number
    return 0;
}

int main(int argc, const char ** argv) {
    std::string filename;
    size_t numbilets = 0;
    std::string parameter;
    if (parse_arguments(argc, argv, filename, numbilets, parameter) != 0) {
        return -1;
    }
    std::ifstream filestream(filename);
    if (filestream.is_open() != true) {
        std::cerr << "Error while open file: " << std::endl << "File <" << filename << "> not found!" << std::endl;
        return -2;
    }
    std::string student_name;
    while (getline(filestream, student_name)) {
        std::cout << student_name << ": " << std::hash<std::string>{}(parameter + student_name) % numbilets + 1 << std::endl;
    }
}




